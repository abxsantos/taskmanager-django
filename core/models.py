from django.db import models


class Todo(models.Model):
    description = models.CharField(max_length=100)
    completed = models.BooleanField(default=False)

    class Meta:
        db_table = "todo_list"

    def __str__(self):
        return self.description

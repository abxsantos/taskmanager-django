import pytest

from core.models import Todo


class TestGetTodoList:
    @pytest.mark.django_db
    def test_available_routes(self, base_url, client):
        """Given the `todos/` endpoint
        when the user sends a get method
        expected to return a 200 status code"""
        response = client.get(base_url + "todos/")
        assert response.status_code == 200


class TestGetTodoDetail:
    @pytest.mark.django_db
    def test_available_detail_routes(self, base_url, client):
        """Given that there is data in `todos/1/` endpoint
        when the user sends a get method
        expected to return a 200 status code"""
        Todo.objects.create(description="Complete more projects", completed=False)
        response = client.get(base_url + "todos/1/")
        assert response.status_code == 200


class TestPostTodoList:
    @pytest.mark.django_db
    def test_post_todo_must_insert_data_and_return_response(
        self, base_url, api_client, default_todo_data
    ):
        """Given data0 and a post method, it should be returned a 201 status code with  correct data"""
        Todo.objects.create(description="Complete more projects", completed=False)
        expected_response = {
            "id": 2,
            "description": "Complete more projects",
            "completed": False,
        }
        response = api_client.post(base_url + "todos/", data=default_todo_data)
        assert response.data == expected_response
        assert response.status_code == 201


class TestPatchTodoDetail:
    @pytest.mark.django_db
    def test_put_todo_must_change_data_and_return_response(self, base_url, api_client):
        """Given a put method in an exising _todo, it should be returned a 200 status code with expected data"""
        Todo.objects.create(description="Complete more projects", completed=False)
        expected_response = {
            "id": 1,
            "description": "Complete more projects",
            "completed": True,
        }
        response = api_client.put(
            base_url + "todos/1/",
            data={"description": "Complete more projects", "completed": True},
        )
        assert response.data == expected_response
        assert response.status_code == 200


class TestDeleteTodoDetail:
    @pytest.mark.django_db
    def test_delete_todo_must_remove_specific_data_and_return_response(
        self, base_url, api_client
    ):
        """Given a delete method it should be returned a 204 status code"""
        Todo.objects.create(description="Complete more projects", completed=False)
        response = api_client.delete(base_url + "todos/1/")
        assert response.status_code == 204

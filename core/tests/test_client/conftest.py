import pytest

from core.models import Todo


@pytest.fixture(scope="function")
def base_url():
    return "http://127.0.0.1:8000/"


@pytest.fixture(scope="function")
def api_client():
    from rest_framework.test import APIClient

    return APIClient()


@pytest.fixture(scope="function")
def default_todo_data():
    default_todo = {"description": "Complete more projects", "complete": False}
    return default_todo

# Todo-api

A simple todo api build on Django to explore the concepts of django-rest-framework, and some CRUD operations as well.<br />
The API is currently deployed at https://taskmanager-django.herokuapp.com/. And the frontend repo can be found [here](https://gitlab.com/abxsantos/task-manager-frontend) which is deployed at https://taskmanager-reactjs.herokuapp.com/. <br />

### Installing

To use the app in your local machine, enter the project folder and use: <br />
`python manage.py runserver`<br/>
This will open a [localhost:8000](http://localhost:8000) to access to the api.<br/>

